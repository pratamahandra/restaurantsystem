<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::resource('foods', 'FoodsController');
Route::resource('tables', 'TablesController');
Route::resource('members', 'MembersController');
Route::resource('drinks', 'DrinksController');
Route::resource('billings', 'BillingsController');
Route::resource('orders', 'OrdersController');
Route::resource('items', 'ItemsController');
Route::get('/getFood',array('as'=>'getFood','uses'=>'OrdersController@getFood'));
Route::get('/findPrice',array('as'=>'findPrice','uses'=>'OrdersController@findPrice'));
Route::get('/findDiscount','BillingsController@findDiscount');
Route::get('/getDrink',array('as'=>'getDrink','uses'=>'OrdersController@getDrink'));
Route::get('/orders', 'OrdersController@index');
Route::patch('/orders', 'OrdersController@update');
Route::post('/orders', 'OrdersController@create');
Route::get('/orders/{{$orders->id}}/edit', 'OrdersController@edit');
Route::post('/orders/edit', 'OrdersController@create');
Route::resource('select', 'TestsController');
Route::get('/select','TestsController@testfunction');
