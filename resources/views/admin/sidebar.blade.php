<div class="col-md-3">
    <div class="panel panel-default panel-flush">
        <div class="panel-heading">
            Sidebar
        </div>

        <div class="panel-body">
            <ul class="nav" role="tablist">
                <li role="presentation">
                    <a href="{{ url('/members') }}">Members</a>
                    <a href="{{ url('/items') }}">Items</a>                    
                    <a href="{{ url('/tables') }}">Tables</a>
                    <a href="{{ url('/orders') }}">Order</a>
                    <a href="{{ url('/billings') }}">Billing</a>
                </li>
            </ul>
        </div>
    </div>
</div>
