@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        @include('admin.sidebar')
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading"><h3>Setup Table</h3></div>
                <div class="panel-body">      
                    {!! Form::open(['method' => 'GET', 'url' => '/members', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                    <div class="input-group">
                        <input type="text" class="form-control" name="search" placeholder="Search...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                    {!! Form::close() !!}            
                    <br/>
                    <br/>
                    <div class="col-md-9">
                        <div class="panel panel-default">
                            <div class="panel-heading">Create New Table</div>
                            <div class="panel-body">
                                <br />                                
                                @if ($errors->any())
                                <ul class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                                @endif

                                {!! Form::open(['url' => '/members', 'class' => 'form-horizontal', 'files' => true]) !!}

                                @include ('members.form')

                                {!! Form::close() !!}

                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row col-lg-13">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-striped" id="">

                                            <table class="table table-borderless">
                                                <thead>
                                                    <tr>
                                                    <th>ID</th><th>Card Number</th><th>Table</th><th>Price</th><th>Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($members as $item)
                                                    <tr>
                                                        <td>{{ $item->id }}</td>
                                                        <td>{{ $item->card_number }}</td><td>{{ $item->name }}</td><td>{{ $item->address }}</td>
                                                        <td>
                                                            <a href="{{ url('/members/' . $item->id) }}" title="View Member"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                                            <a href="{{ url('/members/' . $item->id . '/edit') }}" title="Edit Member"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                                                            {!! Form::open([
                                                            'method'=>'DELETE',
                                                            'url' => ['/members', $item->id],
                                                            'style' => 'display:inline'
                                                            ]) !!}
                                                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                                            'type' => 'submit',
                                                            'class' => 'btn btn-danger btn-xs',
                                                            'title' => 'Delete Member',
                                                            'onclick'=>'return confirm("Confirm delete?")'
                                                            )) !!}
                                                            {!! Form::close() !!}
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            <div class="pagination-wrapper"> {!! $members->appends(['search' => Request::get('search')])->render() !!} </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endsection
