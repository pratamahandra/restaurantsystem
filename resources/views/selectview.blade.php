
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet">
</head>
<body>
    <center>
        <h1>Laravel Dropdown</h1>/
        <!-- Product Name Select Box -->
        <div class="form-group">
         <label class="col-sm-4 control-label">Product Name</label>
         <div class="col-sm-6">
          <select class="food form-control" name="food" style="width:300px"></select>
      </div>
  </div>
</center>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script type="text/javascript">
$( ".food" ).select2({        
    ajax: {
        url: "/orders/index",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
                q: params.term // search term
            };
        },
        processResults: function (data) {
            // parse the results into the format expected by Select2.
            // since we are using custom formatting functions we do not need to
            // alter the remote JSON data
            return {
                results: data
            };
        },
        cache: true
    },
    minimumInputLength: 2
});
</script>
</body> 
</html>



<?php 
//select2 dengan data base mengunakan laravel
/*
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet">
</head>
<body>
<center>
    <h1>Laravel Dropdown</h1>/
    <span>Name : </span>
    <select id="selectid" style="width: 200px" >
        <option></option>
        @foreach($data as $d)
        <option>{{$d->name}}</option>
        @endforeach
    </select>
</center>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script type="text/javascript">
    $("#selectid").select2({
        placeholder:'Select a name'       
    });
</script>
</body> 
</html>
*/?>

<?php 
// select2 biasa
/*  
<!DOCTYPE html>
<html>
<head>
  <script type="text/javascript" src="/assets/js/jquery/jquery-2.1.1.min.js"></script>
  <script type="text/javascript" src="/assets/js/bootstrap/js/bootstrap.min.js"></script>
  <link href="/css/app.css" rel="stylesheet">
  <script type="text/javascript" src="/assets/js/jquery/jquery-2.1.1.min.js"></script>
  <script type="text/javascript" src="/assets/js/bootstrap/js/bootstrap.min.js"></script>    
  <link type="text/css" href="/assets/css/stylesheet.css" rel="stylesheet" media="screen" />
  <link type="text/css" href="/assets/css/select2.css" rel="stylesheet" media="screen" />
  <link type="text/css" href="/assets/css/select2-bootstrap.css" rel="stylesheet" media="screen" />
  <script src="/assets/js/jquery/select2.js" type="text/javascript"></script>    
    <script>
        $(document).ready(function() { 
        $("#e1").select2();
        });
    </script>

</head>
<body>
<center>
<input type="hidden" id="1234">
<select id="e1" style="width:300px">
        <option></option>
        <option value="AL">Alabama</option>
        <option value="Am">Amalapuram</option>
        <option value="An">Anakapalli</option>
        <option value="Ak">Akkayapalem</option>
        <option value="WY">Wyoming</option>
    </select>
</center>
</body>
</html>
*/
?>
