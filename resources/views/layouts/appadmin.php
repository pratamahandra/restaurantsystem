<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Styles -->    
    <link href="/css/app.css" rel="stylesheet">
    <script type="text/javascript" src="/assets/js/jquery/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="/assets/js/bootstrap/js/bootstrap.min.js"></script>    
    <link type="text/css" href="/assets/css/stylesheet.css" rel="stylesheet" media="screen" />
    <link type="text/css" href="/assets/css/select2.css" rel="stylesheet" media="screen" />
    <link type="text/css" href="/assets/css/select2-bootstrap.css" rel="stylesheet" media="screen" />
    <script src="/assets/js/jquery/select2.js" type="text/javascript"></script>    
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
            ]); ?>
        </script>
    </head>
    <body>
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              @yield('content')
          </div>
      </div>
  </div>
  <!-- Scripts -->
  <script src="/js/app.js"></script>
</body>