<div class="form-group {{ $errors->has('billing_number') ? 'has-error' : ''}}">
    {!! Form::label('billing_number', 'Billing Number', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <input type="input"  id="billing_number" readonly="true" value="{{ $billing->billing_number }}" name="billing_number" class="form-control" onchange="sum();"/>
        {!! $errors->first('billing_number', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('sub_total') ? 'has-error' : ''}}">
{!! Form::label('sub_total', 'Sub Total', ['class' => 'col-md-4 control-label']) !!}
<div class="col-md-6">
    <input type="input"  id="sub_total" readonly="true" value="{{ $billing->sub_total }}" name="sub_total" class="form-control" required/>
    {!! $errors->first('sub_total', '<p class="help-block">:message</p>') !!}
</div>
</div><div class="form-group">
{!! Form::label('member_id', 'member_id', ['class' => 'col-md-4 control-label']) !!}
<div class="col-md-6">
    <select class="member_id form-control" name="member_id" id="member_id" onchange="findDiscount();">
        <option value="0" selected="true" disabled="true">Select Member</option>
        @foreach($members as $member => $m)        
        <option value="{!!$m->id!!}">{!!$m->card_number!!} -- {!!$m->name!!}</option>
        @endforeach
    </select>
    {!! $errors->first('member_id', '<p class="help-block">:message</p>') !!}
    
</div>
</div><div class="form-group {{ $errors->has('delivery_amount') ? 'has-error' : ''}}">
{!! Form::label('delivery_amount', 'Delivery Amount', ['class' => 'col-md-4 control-label']) !!}
<div class="col-md-6">
    <input type="text"  id="discount" name="discount" readonly="true" value="0"/>     
    <input type="number"  id="delivery_amount" value="" name="delivery_amount" class="form-control" onchange="sum();" required/>
    {!! $errors->first('delivery_amount', '<p class="help-block">:message</p>') !!}
</div>
</div><div class="form-group {{ $errors->has('delivery_destination') ? 'has-error' : ''}}">
{!! Form::label('delivery_destination', 'Delivery Destination', ['class' => 'col-md-4 control-label']) !!}
<div class="col-md-6">
    {!! Form::textarea('delivery_destination', null, ['class' => 'form-control']) !!}
    {!! $errors->first('delivery_destination', '<p class="help-block">:message</p>') !!}
</div>
</div><div class="form-group {{ $errors->has('payment_method') ? 'has-error' : ''}}">
{!! Form::label('payment_method', 'Payment Method', ['class' => 'col-md-4 control-label']) !!}
<div class="col-md-6">
    {!! Form::select('payment_method', ['cash', 'cash on delivery'], null, ['class' => 'form-control']) !!}
    {!! $errors->first('payment_method', '<p class="help-block">:message</p>') !!}
</div>
</div><div class="form-group {{ $errors->has('sub_total') ? 'has-error' : ''}}">
{!! Form::label('total', 'Total', ['class' => 'col-md-4 control-label']) !!}
<div class="col-md-6">
<input type="hidden"  id="discount" name="discount" readonly="true" value="0"/>   
    <input type="hidden"  id="status" name="status" readonly="true" value="Y"/>   
    <input class="total form-control" id="total" name="total" value="" readonly="true"></input>        
</div>
</div>
<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
<script type="text/javascript">
    function sum() {     
      var sub = $("#sub_total").val();
      var del = $("#delivery_amount").val();
      var dis = $("#discount").val();
      var sum = parseInt(sub)+parseInt(del)-((parseInt(dis)/100)*sub);
      $("#total").val(sum);
  }
  function findDiscount(){
      var id = $('#member_id').val();
      var dataId = {'id':id};
      $.ajax({
        type : 'GET',
        url : '{!!URL("/findDiscount")!!}',
        dataType : 'json',
        data : dataId,
        success : function(data){
          $('#discount').val(data.discount);            
      }
  });      
  };
</script>