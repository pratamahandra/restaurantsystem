@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Billing {{ $billing->id }}</div>
                    <div class="panel-body">

                        <a href="{{ url('/billings') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/billings/' . $billing->id . '/edit') }}" title="Edit Billing"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>                        
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $billing->id }}</td>
                                    </tr>
                                    <tr><th> Billing Number </th><td> {{ $billing->billing_number }} </td></tr><tr><th> Sub Total </th><td> {{ $billing->sub_total }} </td></tr><tr><th> Delivery Amount </th><td> {{ $billing->delivery_amount }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
