@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row">
        @include('admin.sidebar')

        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">Tables</div>
                <div class="panel-body">
                    <a href="{{ url('/tables/create') }}" class="btn btn-success btn-sm" title="Add New Table">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New
                    </a>

                    {!! Form::open(['method' => 'GET', 'url' => '/tables', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                    <div class="input-group">
                        <input type="text" class="form-control" name="search" placeholder="Search...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                    {!! Form::close() !!}

                    <br/>
                    <br/>
                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <thead>
                                <tr>
                                    <th>ID</th><th>Table Number</th><th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($tables as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->table_number }}</td>
                                    <td>
                                        <a href="{{ url('/tables/' . $item->id) }}" title="View Table"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                        <a href="{{ url('/tables/' . $item->id . '/edit') }}" title="Edit Table"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                                        {!! Form::open([
                                        'method'=>'DELETE',
                                        'url' => ['/tables', $item->id],
                                        'style' => 'display:inline'
                                        ]) !!}
                                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete Table',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                        )) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $tables->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-md-9">
            <div class="panel panel-default">
            <div class="panel-heading"><h3>Setup Table</h3></div>
                <div class="panel-body">
                    <a href="{{ url('/tables/create') }}" class="btn btn-success btn-sm" title="Add New Table">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New
                    </a>

                    {!! Form::open(['method' => 'GET', 'url' => '/tables', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                    <div class="input-group">
                        <input type="text" class="form-control" name="search" placeholder="Search...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                    {!! Form::close() !!}

                    <br/>
                    <br/>
                    <form name="setupPaket" action="setupPaket.php" method="post" enctype="multipart/form-data">                            
                        <fieldset>
                           <input name="id" type="hidden" value="">
                           <div class="form-group">
                               <label>Pilih Kategori</label>
                               <select class="form-control" name="id_kategori">                                     
                               </select>
                           </div>
                           <div class="form-group">
                               <label>Nama Paket</label>
                               <input class="form-control" name="nama_paket" type="text" placeholder="Input nama paket" value="">
                           </div>
                           <label>Nama Paket</label>
                           <div class="form-group input-group">
                            <span class="input-group-addon">Rp</span>
                            <input class="form-control" name="harga_paket" type="text" value="">
                            <span class="input-group-addon">,00</span>
                        </div>
                        <div class="form-group">
                           <label>Description</label>
                           <textarea class="form-control" name="ket_paket"></textarea>
                       </div>

                       <div class="btn-group">
                        <input name="Tambah" type="submit" value="Tambah" class="btn">
                    </div>
                    <div class="btn-group">
                        <input name="Edit" type="submit" value="Ubah" class="btn btn-info" data-hint="Klik untuk Hapus Daerah">
                        <input name="Delete" type="submit" value="Hapus" class="btn btn-danger" data-hint="Klik untuk Edit Daerah">
                    </div>                    
                    <div class="btn-group">
                    </fieldset>
                </form>
            </div>


            <div class="panel-body">
                <div class="row col-lg-13">
                    <div class="table-responsive">
                        <table class="table table-hover table-striped" id="">
                            <thead>
                                <tr>
                                    <th>ID</th><th>Table Number</th><th>Actions</th>
                                </tr>
                            </thead>

                            <tbody>                                
                                @foreach($tables as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->table_number }}</td>
                                    <td>
                                        <a href="{{ url('/tables/' . $item->id) }}" title="View Table"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                        <a href="{{ url('/tables/' . $item->id . '/edit') }}" title="Edit Table"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                                        {!! Form::open([
                                        'method'=>'DELETE',
                                        'url' => ['/tables', $item->id],
                                        'style' => 'display:inline'
                                        ]) !!}
                                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete Table',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                        )) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>


        </div>
    </div>        

    @endsection
