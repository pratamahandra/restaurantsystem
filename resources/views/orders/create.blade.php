<div class="container-fluid">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Orders</h3>
    </div>
    <div class="panel-body">
      <form class="form-horizontal" method="POST" action="">
       <div class="form-group">
         <label class="col-sm-2 control-label">Order Number</label>
         <div class="col-sm-3">
           <input type="text"  id="order_number"  name="order_number" class="order_number" required/>
         </div>
       </div>                   
       <div class="form-group">
         <label class="col-sm-2 control-label">Table Number</label>
         <div class="col-sm-4">
           {!! Form::Label('table', 'Table:') !!}
           <select class="form-control" name="table_id" onChange="javascript:food(this)">
            @foreach($tables as $table)
            <option value="{{$table->table_id}}">{{$table->table_number}}</option>
            @endforeach
          </select>
        </div>
      </div>                   
      <div class="form-group">
       <label class="col-sm-2 control-label">Table Number</label>
       <div class="col-sm-4">
       <input type="hidden" id="1234">
        <select  id="e1" style="width:300px">
          <option value="AL">Alabama</option>
          <option value="Am">Amalapuram</option>
          <option value="An">Anakapalli</option>
          <option value="Ak">Akkayapalem</option>
          <option value="WY">Wyoming</option>
        </select>
      </div>
    </div>     
    <div class="form-group" id="table">
     <label class="col-sm-2 control-label">Table</label>
     <div class="col-sm-4">
       <input type="hidden" name="table_id[]" id="table_id1" class="form-control table" required/>
     </div>
     <div class="col-sm-4">
       <a href="javascript:void(0)" onclick="javascript:AddTable()" title="Add New Table" class="btn btn-sm btn-success"><i class="fa fa-plus"></i></a>
     </div>
   </div>

   <div id="data-service"></div>
   <div class="table-responsive">
     <div class="table-responsive">
      <table class="table table-bordered table-hover" id="TData">
        <thead>
          <tr>
            <td colspan="5">
              <label># Detail Order of Food</label>
              <a href="javascript:void(0)" onclick="AddOrder()" title="Add New Order" class="btn pull-right btn-sm btn-success"><i class="fa fa-plus"></i> Add Order</a>
            </td>
          </tr>
          <tr>
            <th>Food</th>
            <th>Quantity</th>                                
            <th>Price</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
    <div class="pull-right">
      <button type="reset" data-toggle="tooltip" title="Reset Form" class="btn btn-warning"><i class="fa fa-refresh"></i> Reset</button>
      <button type="submit" data-toggle="tooltip" title="Save" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
    </div>

  </form>
</div>
</div>

<script type="text/javascript">    
  $("#e1").select2();

  function AddOrder(){
    var row = $('#TData tbody').length;
    if(row>0){
      var index = $('.index').last().attr('id');
      index = parseInt(index);
      row = parseInt(row);
      row = index;
    }
    var html = '<tr id="'+row+'" class="index">';
    html += '<td><input type="hidden" id="food_id'+row+'"  class="form-control" required/></td>';
    html += '<td><input type="number" id="quantity'+row+'" class="form-control"  required/></td>';
    html += '<td><input type="text" id="price'+row+'" onChange="javascript:room(this)"  readonly="true" class="form-control" required/>  </td>';                
    html += '<td><a href="javascript:void(0)" onclick="javascript:deleteRow(this)" class="btn btn-sm btn-danger"><i class="fa fa-times"></i> Delete</a></td>';
    html += '</tr>';        
    $('#TData tbody').append(html);
    $('#food_id'+row).select2({
      ajax: {   
        url: '/order/food',
        dataType: 'json',
        quietMillis: 100,
        data: function (term) {
          return {
                        q: term, // search term
                      };
                    },
                    results: function (data) {
                      var myResults = [];
                      $.each(data, function (index, item) {
                        myResults.push({
                          'id': item.id,
                          'text': item.text
                        });
                      });
                      return {
                        results: myResults
                      };
                    },
                    minimumInputLength: 3
                  }
                });

  }
  function deleteRow(btn) {
    var row = btn.parentNode.parentNode;
    row.parentNode.removeChild(row);
  }

  AddOrder();


  function AddTable(){
    var row = $('.table').length;
    if(row>1){
      var index = $('.table').last().attr('id');
      index = index.substr(10);
      index = parseInt(index);
      row = parseInt(row);
      row = index+1;
    }
    var html = '<div class="form-group" id="table'+row+'">';
    html+= '<label class="col-sm-2 control-label"></label>';
    html+= '<div class="col-sm-4">';
    html+= '<input type="hidden"  name="table_id" id="table_id'+row+'" class="form-control" required/>';
    html+= '</div>';
    html+= '<div class="col-sm-4">';
    html+= '<a href="javascript:void(0)" onclick="javascript:DeleteService('+row+')" title="Delete Service" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></a>';
    html+= '</div>';
    html+= '</div>';
    $('#data-table').append(html);
    $('#table_id1'+row).select2({
      ajax: {
        placeholder: 'Type Table',    
        url: '<?php echo url('/orders/table');?>',
        dataType: 'json',
        quietMillis: 100,
        data: function (term) {
          return {
                        q: term, // search term
                      };
                    },
                    results: function (data) {
                      var myResults = [];
                      $.each(data, function (index, item) {
                        myResults.push({
                          'id': item.id,
                          'text': item.text
                        });
                      });
                      return {
                        results: myResults
                      };
                    },
                    minimumInputLength: 3
                  }
                });

  }

  function DeleteService(x){
    $('#table'+x).remove();
  }

</script>