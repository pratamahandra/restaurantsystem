-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 10, 2017 at 10:03 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `restaurantsystem`
--

-- --------------------------------------------------------

--
-- Table structure for table `billings`
--

CREATE TABLE `billings` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `billing_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_total` decimal(8,2) NOT NULL,
  `delivery_amount` decimal(8,2) DEFAULT NULL,
  `delivery_destination` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `member_id` int(10) UNSIGNED DEFAULT NULL,
  `payment_method` int(2) DEFAULT NULL,
  `total` decimal(8,2) DEFAULT NULL,
  `bill_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `billings`
--

INSERT INTO `billings` (`id`, `user_id`, `billing_number`, `sub_total`, `delivery_amount`, `delivery_destination`, `member_id`, `payment_method`, `total`, `bill_date`, `status`) VALUES
(2, NULL, 'RES-921371', '60000.00', '15000.00', 'Jl. Bandung', NULL, 1, NULL, '2017-04-10 17:04:06', 'Y'),
(3, NULL, 'RES-91238', '30000.00', '10000.00', 'Jl. Kenanga', 1, 0, NULL, '2017-04-10 16:57:33', 'Y'),
(4, NULL, 'RES-1231', '65000.00', NULL, NULL, NULL, NULL, NULL, '2017-04-09 05:04:11', 'N'),
(8, NULL, 'RES-97131', '45000.00', NULL, NULL, NULL, NULL, NULL, '2017-04-10 16:25:16', 'N'),
(14, NULL, 'RES-07123', '55000.00', NULL, NULL, NULL, NULL, NULL, '2017-04-10 16:29:08', 'N'),
(15, 1, 'RES-017231', '80000.00', NULL, NULL, NULL, NULL, NULL, '2017-04-10 18:02:17', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `billing_details`
--

CREATE TABLE `billing_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `billing_id` int(10) UNSIGNED DEFAULT NULL,
  `item_id` int(10) UNSIGNED DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `price` decimal(8,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `billing_details`
--

INSERT INTO `billing_details` (`id`, `billing_id`, `item_id`, `quantity`, `price`) VALUES
(1, 14, 1, 2, '10000.00'),
(2, 14, 2, 1, '15000.00'),
(3, 14, 3, 1, '10000.00'),
(4, 14, 4, 1, '10000.00'),
(5, 15, 1, 2, '10000.00'),
(6, 15, 2, 2, '15000.00'),
(7, 15, 3, 1, '10000.00'),
(8, 15, 4, 1, '10000.00'),
(9, 15, 5, 1, '10000.00');

-- --------------------------------------------------------

--
-- Table structure for table `billing_orders`
--

CREATE TABLE `billing_orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `billing_id` int(10) UNSIGNED DEFAULT NULL,
  `order_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `name`, `category`, `price`, `created_at`, `updated_at`) VALUES
(1, 'Martabak', '0', 10000, '2017-04-04 06:01:25', '2017-04-04 06:01:25'),
(2, 'Sate', '0', 15000, '2017-04-04 06:01:42', '2017-04-04 06:01:42'),
(3, 'Jus Apel', '1', 10000, '2017-04-04 06:02:07', '2017-04-04 06:02:07'),
(4, 'Jus Mangga', '1', 10000, '2017-04-04 06:02:24', '2017-04-04 06:02:24'),
(5, 'Jus Melon', '1', 10000, '2017-04-04 06:02:39', '2017-04-04 06:02:39'),
(6, 'Burger', '0', 10000, '2017-04-04 06:02:59', '2017-04-04 06:02:59');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` int(10) UNSIGNED NOT NULL,
  `card_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `card_number`, `name`, `address`, `discount`, `created_at`, `updated_at`) VALUES
(1, 'RES-018001', 'Budi Utomo', 'Jl. Jend. Sudirman no.46', 10.00, '2017-04-04 06:06:21', '2017-04-06 03:49:34'),
(2, 'RES-018002', 'Reihan Putra', 'Jl. Kenanga Blok B no. 4', 15.00, '2017-04-04 06:08:23', '2017-04-04 06:08:23');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_03_27_210018_create_tables_table', 1),
(4, '2017_04_03_213905_create_items_table', 1),
(5, '2017_03_27_210719_create_payment__methods_table', 2),
(6, '2017_03_27_210248_create_orders_table', 3),
(7, '2017_03_27_210333_create_order__details_table', 4),
(8, '2017_03_27_205920_create_members_table', 5),
(9, '2017_03_27_210801_create_billings_table', 6),
(10, '2017_03_27_210811_create_billing__orders_table', 7),
(11, '2017_03_27_210820_create_billing__details_table', 8);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `order_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_id` int(10) UNSIGNED DEFAULT NULL,
  `order_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT 'N',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `order_number`, `table_id`, `order_date`, `status`, `updated_at`, `created_at`) VALUES
(8, NULL, 'RES-1157', 5, '2017-04-04 07:00:00', NULL, '2017-04-05 01:58:51', '2017-04-05 01:58:51'),
(21, NULL, 'RES-87123', 7, '2017-04-06 07:00:00', NULL, '2017-04-06 11:56:22', '2017-04-06 11:56:22'),
(22, NULL, 'RES-17971', 11, '2017-04-06 07:00:00', NULL, '2017-04-06 23:36:36', '2017-04-06 23:36:36'),
(23, NULL, 'RES-8172', 8, '2017-04-06 07:00:00', NULL, '2017-04-07 03:02:28', '2017-04-07 03:02:28'),
(26, NULL, 'RES-182731', 9, '2017-04-06 07:00:00', NULL, '2017-04-07 06:10:50', '2017-04-07 06:10:50'),
(27, NULL, 'RES-81723', 5, '2017-04-09 07:00:00', NULL, '2017-04-09 11:32:17', '2017-04-09 11:32:17'),
(31, NULL, 'RES-921371', 7, '2017-04-09 07:00:00', NULL, '2017-04-09 11:54:05', '2017-04-09 11:54:05'),
(32, NULL, 'RES-91238', 8, '2017-04-09 07:00:00', NULL, '2017-04-09 11:56:36', '2017-04-09 11:56:36'),
(51, NULL, 'RES-1231', 2, '2017-04-09 07:00:00', NULL, '2017-04-09 12:04:11', '2017-04-09 12:04:11'),
(55, NULL, 'RES-97131', 6, '2017-04-10 07:00:00', NULL, '2017-04-10 23:25:16', '2017-04-10 23:25:16'),
(61, NULL, 'RES-07123', 6, '2017-04-10 07:00:00', NULL, '2017-04-10 23:29:08', '2017-04-10 23:29:08'),
(62, 1, 'RES-017231', 11, '2017-04-10 07:00:00', 'N', '2017-04-11 01:02:17', '2017-04-11 01:02:17');

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED DEFAULT NULL,
  `item_id` int(10) UNSIGNED DEFAULT NULL,
  `price` decimal(8,2) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`id`, `order_id`, `item_id`, `price`, `quantity`) VALUES
(45, 22, 1, '10000.00', 2),
(46, 22, 2, '15000.00', 2),
(47, 22, 3, '10000.00', 2),
(48, 22, 3, '10000.00', 2),
(58, 21, 1, '10000.00', 6),
(59, 21, 2, '15000.00', 5),
(60, 21, 6, '10000.00', 4),
(61, 21, 3, '10000.00', 5),
(79, 26, 1, '10000.00', 1),
(80, 26, 3, '10000.00', 1),
(81, 26, 1, '10000.00', 1),
(90, 23, 1, '10000.00', 2),
(91, 23, 6, '10000.00', 2),
(92, 23, 3, '10000.00', 2),
(93, 23, 2, '15000.00', 1),
(94, 23, 3, '10000.00', 1),
(95, 23, 4, '10000.00', 1),
(96, 23, 2, '15000.00', 10),
(97, 23, 4, '10000.00', 10),
(112, 31, 1, '10000.00', 2),
(113, 31, 2, '15000.00', 2),
(114, 31, 3, '10000.00', 2),
(115, 31, 5, '10000.00', 3),
(116, 32, 2, '15000.00', 2),
(117, 32, 6, '10000.00', 2),
(118, 32, 3, '10000.00', 1),
(119, 32, 3, '10000.00', 1),
(157, 51, 2, '15000.00', 3),
(158, 51, 1, '10000.00', 1),
(159, 51, 3, '10000.00', 1),
(169, 55, 2, '15000.00', 1),
(170, 55, 6, '10000.00', 1),
(171, 55, 4, '10000.00', 2),
(192, 61, 1, '10000.00', 2),
(193, 61, 2, '15000.00', 1),
(194, 61, 3, '10000.00', 1),
(195, 61, 4, '10000.00', 1),
(196, 8, 1, '15000.00', 2),
(197, 8, 2, '10000.00', 3),
(198, 8, 1, '10000.00', 1),
(199, 8, 1, '10000.00', 1),
(200, 8, 3, '10000.00', 1),
(201, 62, 1, '10000.00', 2),
(202, 62, 2, '15000.00', 2),
(203, 62, 3, '10000.00', 1),
(204, 62, 4, '10000.00', 1),
(205, 62, 5, '10000.00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tables`
--

CREATE TABLE `tables` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tables`
--

INSERT INTO `tables` (`id`, `table_number`, `created_at`, `updated_at`) VALUES
(1, 'VIP-A1', '2017-04-04 06:03:26', '2017-04-04 06:03:26'),
(2, 'VIP-A2', '2017-04-04 06:03:36', '2017-04-04 06:03:36'),
(3, 'VIP-A3', '2017-04-04 06:03:40', '2017-04-04 06:03:40'),
(4, 'VIP-A4', '2017-04-04 06:03:46', '2017-04-04 06:03:46'),
(5, 'VIP-A5', '2017-04-04 06:03:50', '2017-04-04 06:03:50'),
(6, 'VIP-B1', '2017-04-04 06:04:02', '2017-04-04 06:04:02'),
(7, 'VIP-B2', '2017-04-04 06:04:05', '2017-04-04 06:04:05'),
(8, 'VIP-B3', '2017-04-04 06:04:09', '2017-04-04 06:04:09'),
(9, 'VIP-B4', '2017-04-04 06:04:13', '2017-04-04 06:04:13'),
(10, 'VIP-B5', '2017-04-04 06:04:27', '2017-04-04 06:04:27'),
(11, 'A1', '2017-04-04 06:04:33', '2017-04-04 06:04:33'),
(12, 'A2', '2017-04-04 06:04:37', '2017-04-04 06:04:37'),
(13, 'A3', '2017-04-04 06:04:49', '2017-04-04 06:04:49'),
(14, 'A4', '2017-04-04 06:05:07', '2017-04-04 06:05:07'),
(15, 'A5', '2017-04-04 06:05:14', '2017-04-04 06:05:14'),
(16, 'B1', '2017-04-04 06:05:26', '2017-04-04 06:05:26'),
(17, 'B2', '2017-04-04 06:05:29', '2017-04-04 06:05:29'),
(18, 'B3', '2017-04-04 06:05:32', '2017-04-04 06:05:32'),
(19, 'B4', '2017-04-04 06:05:35', '2017-04-04 06:05:35'),
(20, 'B5', '2017-04-04 06:05:39', '2017-04-04 06:05:39');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Handra Pratama', 'pratamahandra@gmail.com', '$2y$10$OgChW6jEEG0ANl.asE8jhuaWKMtMIr0hn393HmKOCGzkHtHQYotxq', NULL, '2017-04-10 09:53:16', '2017-04-10 09:53:16');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `billings`
--
ALTER TABLE `billings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `billings_user_id_foreign` (`user_id`),
  ADD KEY `billings_member_id_foreign` (`member_id`);

--
-- Indexes for table `billing_details`
--
ALTER TABLE `billing_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `billing_details_billing_id_foreign` (`billing_id`),
  ADD KEY `billing_details_item_id_foreign` (`item_id`);

--
-- Indexes for table `billing_orders`
--
ALTER TABLE `billing_orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `billing_orders_billing_id_foreign` (`billing_id`),
  ADD KEY `billing_orders_order_id_foreign` (`order_id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_user_id_foreign` (`user_id`),
  ADD KEY `orders_table_id_foreign` (`table_id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_details_order_id_foreign` (`order_id`),
  ADD KEY `order_details_item_id_foreign` (`item_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `tables`
--
ALTER TABLE `tables`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `billings`
--
ALTER TABLE `billings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `billing_details`
--
ALTER TABLE `billing_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `billing_orders`
--
ALTER TABLE `billing_orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=206;
--
-- AUTO_INCREMENT for table `tables`
--
ALTER TABLE `tables`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `billings`
--
ALTER TABLE `billings`
  ADD CONSTRAINT `billings_member_id_foreign` FOREIGN KEY (`member_id`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `billings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `billing_details`
--
ALTER TABLE `billing_details`
  ADD CONSTRAINT `billing_details_billing_id_foreign` FOREIGN KEY (`billing_id`) REFERENCES `billings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `billing_details_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `billing_orders`
--
ALTER TABLE `billing_orders`
  ADD CONSTRAINT `billing_orders_billing_id_foreign` FOREIGN KEY (`billing_id`) REFERENCES `billings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `billing_orders_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_table_id_foreign` FOREIGN KEY (`table_id`) REFERENCES `tables` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `order_details`
--
ALTER TABLE `order_details`
  ADD CONSTRAINT `order_details_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `order_details_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
