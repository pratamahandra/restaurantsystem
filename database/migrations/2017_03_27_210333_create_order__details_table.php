<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->increments('id');
            //kolom untuk foreign Key nya
            $table->unsignedInteger('order_id')->nullable();
            $table->unsignedInteger('item_id')->nullable();            
            $table->decimal('price');           
            $table->integer('quantity');            
        });
        
        //Buat FK tanda dari mana asal kolom order_id
        Schema::table('order_details', function(Blueprint $table){
            $table->foreign('order_id')
                ->references('id')
                ->on('orders')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        
        //Buat FK tanda dari mana asal kolom food_id
        Schema::table('order_details', function(Blueprint $table){
            $table->foreign('item_id')
                ->references('id')
                ->on('items')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        //Buat FK tanda dari mana asal kolom drink_id        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order__details');
    }
}
