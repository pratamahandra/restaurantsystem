<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billing_orders', function (Blueprint $table) {
            $table->increments('id');
            //kolom untuk foreign Key nya
            $table->unsignedInteger('billing_id')->nullable();
            $table->unsignedInteger('order_id')->nullable();
        });
        
        //Buat FK tanda dari mana asal kolom user_id
        Schema::table('billing_orders', function(Blueprint $table){
            $table->foreign('billing_id')
                ->references('id')
                ->on('billings')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        
        //Buat FK tanda dari mana asal kolom table_id
        Schema::table('billing_orders', function(Blueprint $table){
            $table->foreign('order_id')
                ->references('id')
                ->on('orders')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billing__orders');
    }
}
