<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billings', function (Blueprint $table) {
            $table->increments('id');
            //kolom untuk foreign Key nya
            $table->unsignedInteger('user_id')->nullable();            
            $table->string('billing_number');         
            $table->decimal('sub_total');         
            $table->string('delivery_status');         
            $table->decimal('delivery_amount');        
            $table->string('delivery_destination');                     
            $table->unsignedInteger('member_id')->nullable();
            $table->decimal('total');           
            $table->unsignedInteger('payment_method_id')->nullable();
            $table->timestamp('bill_date');
        });
        
        //Buat FK tanda dari mana asal kolom user_id
        Schema::table('billings', function(Blueprint $table){
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        
        //Buat FK tanda dari mana asal kolom table_id
        Schema::table('billings', function(Blueprint $table){
            $table->foreign('member_id')
                ->references('id')
                ->on('members')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        
        //Buat FK tanda dari mana asal kolom table_id
        Schema::table('billings', function(Blueprint $table){
            $table->foreign('payment_method_id')
                ->references('id')
                ->on('payment_methods')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billings');
    }
}
