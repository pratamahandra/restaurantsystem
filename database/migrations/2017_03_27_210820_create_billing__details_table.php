<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billing_details', function (Blueprint $table) {
            $table->increments('id');
            //kolom untuk foreign Key nya
            $table->unsignedInteger('billing_id')->nullable();
            $table->unsignedInteger('item_id')->nullable();            
            $table->integer('quantity');
            $table->decimal('price');
            $table->timestamps();
            
        });
        
        //Buat FK tanda dari mana asal kolom billing_id
        Schema::table('billing_details', function(Blueprint $table){
            $table->foreign('billing_id')
                ->references('id')
                ->on('billings')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        
        //Buat FK tanda dari mana asal kolom food_id
        Schema::table('billing_details', function(Blueprint $table){
            $table->foreign('item_id')
                ->references('id')
                ->on('items')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billing__details');
    }
}
