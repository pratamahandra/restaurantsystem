<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            //kolom untuk foreign Key nya
            $table->unsignedInteger('user_id')->nullable();            
            $table->string('billing_number');         
            $table->unsignedInteger('table_id')->nullable();
            $table->decimal('price');           
            $table->timestamp('order_date');
        });
        
        //Buat FK tanda dari mana asal kolom user_id
        Schema::table('orders', function(Blueprint $table){
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        
        //Buat FK tanda dari mana asal kolom table_id
        Schema::table('orders', function(Blueprint $table){
            $table->foreign('table_id')
                ->references('id')
                ->on('tables')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
