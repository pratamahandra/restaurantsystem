<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Billing extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'billings';
    public $timestamps = false;
    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['billing_number', 'sub_total','delivery_amount', 'delivery_destination', 'member_id', 'payment_method' , 'bil_date','status'];


    public function billing_details()
    {
        return $this->belongsTo('App\Billing_details');
    }
    public function billing_orders()
    {
        return $this->belongsTo('App\Billing_orders');
    }
    
}
