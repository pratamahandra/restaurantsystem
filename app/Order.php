<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'orders';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['order_number', 'table_id', 'order_date'];

  
    public function order_details()
    {
        return $this->belongsTo('App\Order_details');
    }
    public function billing_orders()
    {
        return $this->belongsTo('App\Billing_orders');
    }
}
