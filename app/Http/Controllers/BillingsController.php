<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Billing;
use App\Order;
use App\Member;
use Illuminate\Http\Request;
use Session;

class BillingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $billings = Billing::where('billing_number', 'LIKE', "%$keyword%")
            ->orWhere('sub_total', 'LIKE', "%$keyword%")
            ->orWhere('delivery_amount', 'LIKE', "%$keyword%")
            ->orWhere('delivery_destination', 'LIKE', "%$keyword%")
            ->orWhere('payment_method', 'LIKE', "%$keyword%")

            ->paginate($perPage);
        } else {
            $billings = Billing::where('status','N')
            ->paginate($perPage);
        }

        return view('billings.index', compact('billings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('billings.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->all();
        
        Billing::create($requestData);

        Session::flash('flash_message', 'Billing added!');

        return redirect('billings');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $billing = Billing::findOrFail($id);

        return view('billings.show', compact('billing'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $billing = Billing::findOrFail($id);
        $members = Member::all();
        return view('billings.edit', compact('billing', $members, 'members'));
    }

    public function findDiscount(Request $request)
    {
        $data = Member::select('discount')->where('id',$request->id)->first();
        return response()->json($data);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {

        $requestData = $request->all();
//        dd($request->all());
        ///$status = $request->status;
        $billing = Billing::findOrFail($id);
        $billing->update($requestData);

        Session::flash('flash_message', 'Billing updated!');

        return redirect('billings');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Billing::destroy($id);

        Session::flash('flash_message', 'Billing deleted!');

        return redirect('billings');
    }
}
