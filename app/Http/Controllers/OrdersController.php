<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Order;
use App\Table;
use App\Item;
use App\Order_detail;
use App\Billing_detail;
use App\Billing;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Routing\UrlGenerator;
use DateTime;
use Session;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 5;

        if (!empty($keyword)) {
            $orders = Order::where('order_number', 'LIKE', "%$keyword%")
            ->orWhere('table_id', 'LIKE', "%$keyword%")            
            ->paginate($perPage);
        } else {
            $orders = Order::paginate($perPage);
        }
        $tables = DB::Table('tables')->get(); //get data dari form  table
        //return view('orders.create');
        $foods  = Item::where('category','0')->get();
        $drinks = Item::where('category','1')->get();
        return view('orders.index', compact('orders','foods' , $foods ,'drinks' , $drinks , 'tables' , $tables));
        

    }   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */   

    public function create(Request $request)

    {                

        $id_edit = $request->input('id_edit');  
        

        if($id_edit){
            //print_r('ini update');
            $bs = Order_detail::where('order_id',$id_edit);
            $bs->delete();
            $order_details= new  Order_detail;            
            $id = $id_edit;
            foreach ($request->item as $keyy => $vl) 
            {
                $data = array(
                    'order_id'=>$id,
                    'item_id'=>$vl,
                    'price'=>$request->pricei [$keyy],
                    'quantity'=>$request->quantityi [$keyy]
                    );

                Order_detail::insert($data);

            }
            foreach ($request->food as $key => $value) 
            {
                $dataf = array(
                    'order_id'=>$id,
                    'item_id'=>$value,
                    'price'=>$request->price [$key],
                    'quantity'=>$request->quantity [$key]
                    );
                Order_detail::insert($dataf);

            }
            foreach ($request->drink as $k => $v) 
            {
                $datad = array(
                    'order_id'=>$id,
                    'item_id'=>$v,
                    'price'=>$request->priced [$k],
                    'quantity'=>$request->quantityd [$k]
                    );
                Order_detail::insert($datad);

            }
            return redirect('orders');
        }
        else{
            $orders= new Order;
            $orders->order_number = $request->order_number;
            $orders->user_id = Auth::user()->id;
            $orders->table_id = $request->table_id;    
            $today = new DateTime();
            $today = date('Y-m-d');
            $orders->order_date = $today;        
            $total_f=0;
            $total_d=0;
            if($orders->save())
            {
                $id = $orders->id;
                foreach ($request->food as $key => $value) 
                {
                    $data = array(
                        'order_id'=>$id,
                        'item_id'=>$value,
                        'price'=>$request->price [$key],
                        'quantity'=>$request->quantity [$key],
                        );
                    $sub_f=$request->quantity [$key]*$request->price [$key];
                    $total_f=$sub_f+$total_f;

                    Order_detail::insert($data);

                }
                
                foreach ($request->drink as $k => $v) 
                {
                    $datad = array(
                        'order_id'=>$id,
                        'item_id'=>$v,
                        'price'=>$request->priced [$k],
                        'quantity'=>$request->quantityd [$k],                                                
                        );
                    $sub_d=$request->quantityd [$k]*$request->priced [$k];
                    $total_d=$sub_d+$total_d;                        
                    Order_detail::insert($datad);
                }                
            }
            $billing= new Billing;
            $billing->billing_number = $request->order_number; 
            $billing->user_id = Auth::user()->id; 
            $TOTAL=$total_d+$total_f;            
            $billing->sub_total = $TOTAL;
            $billing->status ='N';    
            if($billing->save())
            {
                $id = $orders->id;
                $id_bill = $billing->id;
                foreach ($request->food as $key => $value) 
                {
                    $data = array(
                        'billing_id'=>$id_bill,                        
                        'item_id'=>$value,
                        'price'=>$request->price [$key],
                        'quantity'=>$request->quantity [$key],
                        );
                    $sub_f=$request->quantity [$key]*$request->price [$key];
                    $total_f=$sub_f+$total_f;

                    Billing_detail::insert($data);

                }
                
                foreach ($request->drink as $k => $v) 
                {
                    $datad = array(
                        'billing_id'=>$id_bill,
                        'item_id'=>$v,
                        'price'=>$request->priced [$k],
                        'quantity'=>$request->quantityd [$k],                                                
                        );
                    $sub_d=$request->quantityd [$k]*$request->priced [$k];
                    $total_d=$sub_d+$total_d;                        
                    Billing_detail::insert($datad);
                }                
            }        
            $orders->save();             
            $billing->save();             
            return redirect('orders');
        }
        
    }

    public function findPrice(Request $request)
    {
        $data = Item::select('price')->where('id',$request->id)->first();
        return response()->json($data);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $food_id = Input::get('food_id');
        for($j=0;$j<count($food_id);$j++){
            $cek_r = Order::where('food_id','=',$food_id[$j])->first();
            if(empty($cek_r)){
                $order = new Order;
                $d_food = Food::find($food_id[$j]);                    
                $order->order_id = $id;
                $order->food_id = $food_id[$j];
                $order->price = $d_food->type->price;
                $order->save();
                $d_food->save();                    
            }
        }               

        Session::flash('flash_message', 'Order added!');

        return redirect('orders');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $order = Order::findOrFail($id);

        return view('orders.show', compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {        
        $tables = DB::Table('tables')->get(); //get data dari form  table
        //return view('orders.create');
        $foods  = Item::where('category','0')->get();
        $drinks = Item::where('category','1')->get();
        $items = Item::all();
        $orders = Order::findOrFail($id);
        $order_d = Order_detail::where('order_id','=',$id)->get();        
        $data = Order::find($id);                
        $options = array(
            'data'=>$data,
            'order_detail'=>$order_d,            
            );
        
        return view('orders.edit', compact('orders','foods' , $foods ,'drinks' , $drinks , 'tables' , $tables, $options, $order_d, 'order_d', $items ,'items' ));        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {

        $requestData = $request->all();
        
        $order = Order::findOrFail($id);
        $order->update($requestData);

        Session::flash('flash_message', 'Order updated!');

        return redirect('orders');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Order::destroy($id);        
        Session::flash('flash_message', 'Order deleted!');

        return redirect('orders');
    }
}
