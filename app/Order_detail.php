<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order_detail extends Model
{
	public $timestamps = false;
    protected $fillable = ['id','order_id','item_id','price','quantity'];

    public function ManyOrders()
    {
    	return $this->hasMany('App\Orders');
    }

}
