<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Billing_detail extends Model
{
	public $timestamps = false;
    protected $fillable = ['id','billing_id','item_id','price','quantity'];

    public function ManyBillings()
    {
    	return $this->hasMany('App\billing');
    }

}
